package com.eoscode.springapitools.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {
        "com.eoscode.springapitools"
})
public class SpringApiToolsScan {
}
